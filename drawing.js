function drawLine(fromX, fromY, toX, toY, colour, width)
{
  let context = screen.getContext("2d");
  context.strokeStyle = colour;
  context.lineWidth = width;
  context.beginPath();
  context.moveTo(fromX, fromY);
  context.lineTo(toX, toY);
  context.stroke();
}

function clearScreen(colour)
{
  let context = screen.getContext("2d");
  context.fillStyle = colour;
  context.rect(0, 0, screenWidth, screenHeight);
  context.fill();
}
