const WebSocket = require('ws');
const wss = new WebSocket.Server({ port: 8080 });

// TEMP
const maxPlayers = 6;

var rooms = {};

function broadcastToRoom(code, data)
{
  wss.clients.forEach(function each(client) {
    if(client.readyState === WebSocket.OPEN && client.code === code)
    {
      client.send(data);
    }
  });
}

// WEBSOCKET STUFF!
wss.on('connection', function connection(ws) {
  console.log('connection received!' + ws);

  ws.on('message', function incoming(message) {
    console.log('received: ' + message);

    let data;
    try {
      data = JSON.parse(message);
    }
    catch(err)
    {
      console.error("Failed to parse JSON");
      return;
    }

    switch(data.type)
    {
      case "l":
        if(ws.code !== "")
        {
          broadcastToRoom(ws.code,'{\"type\":\"l\", \"data\":[' + data.data +'], \"id\":' + ws.playerId + '}');
        }
        break;
      case "player":
        if(!rooms[data.code])
        {
          ws.playerId = 0;
          rooms[data.code] = {};
          rooms[data.code].players = [];
          rooms[data.code].players.push(data.name);
        }
        else if (rooms[data.code].players.length < maxPlayers)
        {
          ws.playerId = rooms[data.code].players.length;
          rooms[data.code].players.push(data.name);
        }
        else {
          console.log("room full!");
          // Tell the player the room is full!
          // for now just break so they stay not in a room
          break;
        }
        ws.send('{\"type\":\"id\", \"id\":' + ws.playerId + '}');
        ws.code = data.code;
        ws.name = data.name;
        break;
      case "clear":
        broadcastToRoom(ws.code, '{\"type\":\"clear\", \"colour\":\"#fff\"}');
        break;
      default:
        break;
    }
  });

  ws.code = "";
  ws.name = "";
  ws.send('{\"type\":\"accepted\", \"data\":\"Connection Accepted!\"}');
  ws.send('{\"type\":\"clear\", \"colour\":\"#fff\"}');
});

console.log('starting server!');
