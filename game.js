screen = document.getElementById('screen');

function getPixels(){
  screen.height = 500;
  screen.width = 500;
}


window.onmousedown = function(e){
  //if the left click button was pressed
  console.log(e.which);
  // left click
  if(e.which == 1){
    mouseDown = true;
  }
  // right click
  else if(e.which == 3){

  }
}

//window on mouse up not screen so the game knows if they take the mouse up
//outside the window
window.onmouseup = function(e){
  //make sure the left button was unclicked
  if(e.which == 1){
    mouseDown = false;
  }
}

screen.onmouseout = function(e){
  //mouseDown = false;
}

screen.onmousemove = function(e){
  let x = e.clientX - screen.getBoundingClientRect().left;
  let y = e.clientY - screen.getBoundingClientRect().top;

  if(mouseDown)
  {
    drawLine(mouseCoords.x, mouseCoords.y, x, y, colour, thickness);
    // send a message to the server through the socket if it's open
    if(socket)
    {
      data = [mouseCoords.x, mouseCoords.y, x, y, thickness];
      socket.send('{\"type\":\"l\", \"data\":[' + data + ']}');
    }
  }

  mouseCoords.x = x;
  mouseCoords.y = y;
}

//stuff for keyboard input
window.onkeydown = function(e){
  let key = e.keyCode;
  switch(key){
    default:
      break;
  }
}

document.getElementById('thickness').oninput = function()
{
  thickness = this.value;
}

document.getElementById('connect').onclick = function(e)
{
  // if there is already a socket try to close it
  if(socket)
  {
    socket.close();
  }
  // try connecting to localhost
  OpenSocket("ws://127.0.0.1:8080/",
   document.getElementById('code').value,
   document.getElementById('playerName').value);
}

document.getElementById('clear').onclick = function(e)
{
  // send
  if(socket)
  {
    socket.send('{\"type\":\"clear\"}');
  }
}

getPixels();
