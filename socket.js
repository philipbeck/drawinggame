function OpenSocket(address, code, name)
{
  console.log("connecting to "  + address);
  socket = new WebSocket(address);

  socket.onopen = function(event)
  {
    console.log("connecting to " + address + " with " + code);
    //TODO have a place to enter the code
    socket.send("{\"type\":\"player\", \"code\":\"" + code + "\", \"name\":\"" + name + "\"}");
  }

  socket.onmessage = function(event)
  {
    console.log("received data " + event.data);

    //let dataString = '{' + event.data + '}';
    let data;

    try {
      data = JSON.parse(event.data);
    }
    catch(err)
    {
      console.error("Failed to parse JSON");
      return;
    }

    switch(data.type)
    {
      case "accepted":
      default:
        break;
      case "l":
        drawLine(data.data[0], data.data[1], data.data[2], data.data[3], colours[data.id], data.data[4]);
        break;
      case "clear":
        clearScreen(data.colour);
        break;
      case "id":
        colour = colours[data.id];
        break;
    }
  }

  socket.onclose = function(event)
  {
    console.log("socket closed");
    ownColour = "#000";
  }

  socket.onerror = function(event)
  {
    alert("Socket Error: " + event.message);
    socket = false;
  }
}
